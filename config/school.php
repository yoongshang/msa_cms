<?php

return [
    'college' => [
        0 => ['full'=> '人文學院', 'short'=> '人院']
        , 1 => ['full'=> '教育學院', 'short'=> '教院']
        , 2 => ['full' => '科技學院', 'short' => '科院']
        , 3 => ['full' => '管理學院', 'short' => '管院']
    ],

    'department' => [
        0 => [
            'college' => config('school.college.0')
            , 'full'  => '中國語文學系'
            , 'short' => '中文系']
        , 1 => [
            'college' => config('school.college.0')
            , 'full'  => '外國語文學系'
            , 'short' => '外語系']
        , 2 => [
            'college' => config('school.college.0')
            , 'full'  => '社會政策與社會工作學系'
            , 'short' => '社工系']
        , 3 => [
            'college' => config('school.college.0')
            , 'full'  => '公共行政與政策學系'
            , 'short' => '公行系']
        , 4 => [
            'college' => config('school.college.0')
            , 'full'  => '歷史學系'
            , 'short' => '歷史系']
        , 5 => [
            'college' => config('school.college.0')
            , 'full'  => '東南亞學系東南亞組'
            , 'short' => '東南亞系東南亞組']
        , 6 => [
            'college' => config('school.college.0')
            , 'full'  => '東南亞學系人類學組'
            , 'short' => '東南亞系人類學組']
        , 7 => [
            'college' => config('school.college.1')
            , 'full'  => '國際文教與比較教育學系'
            , 'short' => '國比系']
        , 8 => [
            'college' => config('school.college.1')
            , 'full'  => '教育政策與行政學系'
            , 'short' => '教政系']
        , 9 => [
            'college' => config('school.college.1')
            , 'full'  => '諮商心理與人力資源發展學系諮商心理組'
            , 'short' => '諮人系諮商心理組']
        , 10 => [
            'college' => config('school.college.1')
            , 'full'  => '諮商心理與人力資源發展學系終身學習與人力資源發展組'
            , 'short' => '諮人系終身學習與人力資源發展組']
        , 11 => [
            'college' => config('school.college.2')
            , 'full'  => '資訊工程學系'
            , 'short' => '資工系']
        , 12 => [
            'college' => config('school.college.2')
            , 'full'  => '土木工程學系'
            , 'short' => '土木系']
        , 13 => [
            'college' => config('school.college.2')
            , 'full'  => '電機工程學系'
            , 'short' => '電機系']
        , 14 => [
            'college' => config('school.college.2')
            , 'full'  => '應用化學系'
            , 'short' => '應化系']
        , 15 => [
            'college' => config('school.college.3')
            , 'full'  => '財務金融學系'
            , 'short' => '財金系']
        , 16 => [
            'college' => config('school.college.3')
            , 'full'  => '經濟學系'
            , 'short' => '經濟系']
        , 17 => [
            'college' => config('school.college.3')
            , 'full'  => '國際企業學系'
            , 'short' => '國企系']
        , 18 => [
            'college' => config('school.college.3')
            , 'full'  => '資訊管理學系'
            , 'short' => '資管系']
        , 19 => [
            'college' => config('school.college.3')
            , 'full'  => '觀光休閒與餐旅管理學系觀光休閒組'
            , 'short' => '觀餐系觀光組']
        , 20 => [
            'college' => config('school.college.3')
            , 'full'  => '觀光休閒與餐旅管理學系餐旅管理組'
            , 'short' => '觀餐旅系餐旅組']
    ]
];
