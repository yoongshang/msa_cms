<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Validator;

class ValidatorTest extends TestCase
{
    /**
     * Test isEmailValid(), if empty string
     */
    public function testIsEmailValidIfEmpty()
    {
        $this->assertFalse(Validator::isEmailValid(''));
    }

    /**
     * Test isEmailValid(), if valid email
     */
    public function testIsEmailValidIfValid()
    {
        $email = 'myemail@msa.com';
        $this->assertTrue(Validator::isEmailValid($email));
    }

    /**
     * Test isEmailValid(), if invalid email
     */
    public function testIsEmailValidIfInvalid()
    {
        $email = 'myemail';
        $this->assertFalse(Validator::isEmailValid($email));
    }

    /**
     * Test isStudentNumberValid(), if empty string
     */
    public function testIsStudentNumberValidIfEmpty()
    {
        $this->assertFalse(Validator::isStudentNumberValid(''));
    }

    /**
     * Test isStudentNumberValid(), if valid student number
     */
    public function testIsStudentNumberValidIfValid()
    {
        $number = '102321056';
        $this->assertTrue(Validator::isStudentNumberValid($number));

        $number = '86421056';
        $this->assertTrue(Validator::isStudentNumberValid($number));

        $number = '107121056';
        $this->assertTrue(Validator::isStudentNumberValid($number));
    }

    /**
     * Test isStudentNumberInvalid(), if invalid student number
     */
    public function testIsStudentNumberValidIfInvalid()
    {
        $number = '108321056';
        $this->assertFalse(Validator::isStudentNumberValid($number));

        $number = '83421056';
        $this->assertFalse(Validator::isStudentNumberValid($number));

        $number = '106521056';
        $this->assertFalse(Validator::isStudentNumberValid($number));
    }

    /**
     * Test isRocValid(), if empty string
     */
    public function testIsRocValidIfEmpty()
    {
        $this->assertFalse(Validator::isRocValid(''));
    }

    /**
     * Test isRocValid(), if valid ROC number
     */
    public function testIsRocValidIfValid()
    {
        $roc = 'MC01234567';
        $this->assertTrue(Validator::isRocValid($roc));

        $roc = 'MD01234567';
        $this->assertTrue(Validator::isRocValid($roc));

        $roc = 'A123456789';
        $this->assertTrue(Validator::isRocValid($roc));

        $roc = 'K234567890';
        $this->assertTrue(Validator::isRocValid($roc));
    }

    /**
     * Test isRocValid(), if invalid ROC number
     */
    public function testIsRocValidIfInvalid()
    {
        $roc = 'ME01234567';
        $this->assertFalse(Validator::isRocValid($roc));

        $roc = 'MF01234567';
        $this->assertFalse(Validator::isRocValid($roc));

        $roc = 'A323456789';
        $this->assertFalse(Validator::isRocValid($roc));

        $roc = 'A123456789sad';
        $this->assertFalse(Validator::isRocValid($roc));
    }

    /**
     * Test isPhoneNumberValid, if empty string
     */
    public function testIsPhoneNumberValidIfEmpty()
    {
        $this->assertFalse(Validator::isPhoneNumberValid(''));
    }

    /**
     * Test isPhoneNumberValid, if valid phone number
     */
    public function testIsPhoneNumberValidIfValid()
    {
        $number = '+886912345678';
        $this->assertTrue(Validator::isPhoneNumberValid($number));

        $number = '0912345678';
        $this->assertTrue(Validator::isPhoneNumberValid($number));

        $number = '091-2345-678';
        $this->assertTrue(Validator::isPhoneNumberValid($number));
    }

    /**
     * Test isPhoneNumberValid, if invalid phone number
     */
    public function testIsPhoneNumberValidIfInvalid()
    {
        $number = '-0912345678';
        $this->assertFalse(Validator::isPhoneNumberValid($number));

        $number = '+-0912345678';
        $this->assertFalse(Validator::isPhoneNumberValid($number));

        $number = '091234567801234567890';
        $this->assertFalse(Validator::isPhoneNumberValid($number));
    }
}
