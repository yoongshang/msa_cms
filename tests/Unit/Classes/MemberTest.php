<?php

namespace Tests\Unit\Classes;

use Tests\TestCase;
use App\Classes\Member;
use stdClass;

class MemberTest extends TestCase
{
    /**
     * Test Setter and getter of class
     */
    public function testSetterAndGetter()
    {
        $member = new Member();
        $member->setId(1);
        $member->setStudentNumber('102321056');
        $member->setRoc('MC01234567');
        $member->setDepartment(1);
        $member->setPassword('$2a$04$5wnkdvLUdRblXAnvOQ10jOmnRffM6h64P4kKfUgan8R16zNNil/qW');
        $member->setNameCn('王大垂');
        $member->setNameEn('Wang Da Cui');
        $member->setGender(Member::GENDER_MALE);
        $member->setBirthday('1996-12-27');
        $member->setEmail('wang@gmail.com');
        $member->setPhoneTw('0912345678');
        $member->setPhonePor('+6012345678');
        $member->setAddressTw('南投縣埔里鎮大學路1號');
        $member->setAddressPor('27, Lengkok Kapal, Penang, Malaysia.');
        $member->setActive(true);
        $member->setCreatedTime('2017-11-21 23:00');
        $member->setUpdatedTime('2017-11-31 19:05');

        $this->assertEquals(1, $member->getId());
        $this->assertEquals('102321056', $member->getStudentNumber());
        $this->assertEquals('MC01234567', $member->getRoc());
        $this->assertEquals(1, $member->getDepartment());
        $this->assertEquals('$2a$04$5wnkdvLUdRblXAnvOQ10jOmnRffM6h64P4kKfUgan8R16zNNil/qW'
            , $member->getPassword());
        $this->assertEquals('王大垂', $member->getNameCn());
        $this->assertEquals('Wang Da Cui', $member->getNameEn());
        $this->assertEquals(1, $member->getGender());
        $this->assertEquals('1996-12-27', $member->getBirthday());
        $this->assertEquals('wang@gmail.com', $member->getEmail());
        $this->assertEquals('0912345678', $member->getPhoneTw());
        $this->assertEquals('+6012345678', $member->getPhonePor());
        $this->assertEquals('南投縣埔里鎮大學路1號', $member->getAddressTw());
        $this->assertEquals('27, Lengkok Kapal, Penang, Malaysia.', $member->getAddressPor());
        $this->assertEquals(true, $member->isActive());
        $this->assertEquals('2017-11-21 23:00', $member->getCreatedTime());
        $this->assertEquals('2017-11-31 19:05', $member->getUpdatedTime());
    }

    /**
     * Test load from object
     */
    public function testLoadFromObject()
    {
        $object = new stdClass();
        $object->ixMember = 1;
        $object->sStudentNumber = '102321056';
        $object->sRoc = 'MC01234567';
        $object->iDepartment = 1;
        $object->sPassword = '$2a$04$5wnkdvLUdRblXAnvOQ10jOmnRffM6h64P4kKfUgan8R16zNNil/qW';
        $object->sNameCn = '王大垂';
        $object->sNameEn = 'Wang Da Cui';
        $object->nGender = Member::GENDER_MALE;
        $object->dtBirthday = '1996-12-27';
        $object->sEmail = 'wang@gmail.com';
        $object->sPhoneTw = '0912345678';
        $object->sPhonePor = '+6012345678';
        $object->sAddressTw = '南投縣埔里鎮大學路1號';
        $object->sAddressPor = '27, Lengkok Kapal, Penang, Malaysia.';
        $object->fActive = true;
        $object->dtCreated = '2017-11-21 23:00';
        $object->dtUpdated = '2017-11-31 19:05';

        $member = new Member();
        $member->loadFromObject($object);

        $this->assertEquals(1, $member->getId());
        $this->assertEquals('102321056', $member->getStudentNumber());
        $this->assertEquals('MC01234567', $member->getRoc());
        $this->assertEquals(1, $member->getDepartment());
        $this->assertEquals('$2a$04$5wnkdvLUdRblXAnvOQ10jOmnRffM6h64P4kKfUgan8R16zNNil/qW'
            , $member->getPassword());
        $this->assertEquals('王大垂', $member->getNameCn());
        $this->assertEquals('Wang Da Cui', $member->getNameEn());
        $this->assertEquals(1, $member->getGender());
        $this->assertEquals('1996-12-27', $member->getBirthday());
        $this->assertEquals('wang@gmail.com', $member->getEmail());
        $this->assertEquals('0912345678', $member->getPhoneTw());
        $this->assertEquals('+6012345678', $member->getPhonePor());
        $this->assertEquals('南投縣埔里鎮大學路1號', $member->getAddressTw());
        $this->assertEquals('27, Lengkok Kapal, Penang, Malaysia.', $member->getAddressPor());
        $this->assertEquals(true, $member->isActive());
        $this->assertEquals('2017-11-21 23:00', $member->getCreatedTime());
        $this->assertEquals('2017-11-31 19:05', $member->getUpdatedTime());
    }

    /**
     * @expectedException \TypeError
     */
    public function testLoadFromObjectIfNoContent()
    {
        $member = new Member();
        $member->loadFromObject(null);
    }
}
