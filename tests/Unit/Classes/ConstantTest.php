<?php

namespace Tests\Unit\Classes;

use Tests\TestCase;
use App\Classes\Constant;

class ConstantTest extends TestCase
{
    /**
     * Test Constant constructor
     */
    public function testConstructor()
    {
        $constant = new Constant(1, 'constantA');

        $this->assertEquals(1, $constant->getCode());
        $this->assertEquals('constantA', $constant->getName());
    }

    /**
     * Test Getter and Setter
     */
    public function testGetterAndSetter()
    {
        $constant = new Constant();

        $constant->setCode(1);
        $constant->setName('constantA');

        $this->assertEquals(1, $constant->getCode());
        $this->assertEquals('constantA', $constant->getName());
    }
}
