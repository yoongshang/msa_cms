<?php

use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Member')->insert([
            'sStudentNumber' => '102321056'
            , 'sRoc' => 'MC00123456'
            , 'iDepartment' => 1
            , 'sPassword' => bcrypt('pass1')
            , 'sNameCn' => '王大捶'
            , 'sNameEn' => 'Wang Da Cui'
            , 'nGender' => 1
            , 'dtBirthday' => '1994-01-01'
            , 'sEmail' => 'abc@gmail.com'
            , 'sPhoneTw' => '0961234567'
            , 'sPhonePor' => '+6043312345'
            , 'sAddressTw' => '南投縣埔里鎮大學路1號'
            , 'sAddressPor' => '27, Lengkok Kapal, Penang, Malaysia.'
            , 'fActive' => 1
            , 'dtCreated' => date("Y-m-d h:i:s", strtotime('2017-11-21 01:00:00'))
            , 'dtUpdated' => date("Y-m-d h:i:s", strtotime('2017-11-31 02:05:15'))
        ]);
        DB::table('Member')->insert([
            'sStudentNumber' => '102322050'
            , 'sRoc' => 'MD00123456'
            , 'iDepartment' => 2
            , 'sPassword' => bcrypt('pass2')
            , 'sNameCn' => '王小可'
            , 'sNameEn' => 'Wang Xiao Ke'
            , 'nGender' => 1
            , 'dtBirthday' => '1994-11-05'
            , 'sEmail' => 'dfg@gmail.com'
            , 'sPhoneTw' => '0981234567'
            , 'sPhonePor' => '+60123312345'
            , 'sAddressTw' => '南投縣埔里鎮大學路501號'
            , 'sAddressPor' => '27, Lengkok Kapal, Kuala Lumpur, Malaysia.'
            , 'fActive' => 1
            , 'dtCreated' => date("Y-m-d h:i:s", strtotime('2017-11-23 01:00:00'))
            , 'dtUpdated' => date("Y-m-d h:i:s", strtotime('2017-11-31 02:05:15'))
        ]);
        DB::table('Member')->insert([
            'sStudentNumber' => '106322051'
            , 'sRoc' => 'MD00123478'
            , 'iDepartment' => 3
            , 'sPassword' => bcrypt('pass3')
            , 'sNameCn' => '劉彥希'
            , 'sNameEn' => 'Lau Yean Sai'
            , 'nGender' => 2
            , 'dtBirthday' => '1996-12-27'
            , 'sEmail' => 'sai@gmail.com'
            , 'sPhoneTw' => '0981234987'
            , 'sPhonePor' => '+60123312765'
            , 'sAddressTw' => '南投縣埔里鎮大學路502號'
            , 'sAddressPor' => '81, Lengkok Kapal, Kuala Lumpur, Malaysia.'
            , 'fActive' => 1
            , 'dtCreated' => date("Y-m-d h:i:s", strtotime('2017-11-24 01:11:00'))
            , 'dtUpdated' => date("Y-m-d h:i:s", strtotime('2017-11-31 02:05:15'))
        ]);
    }
}
