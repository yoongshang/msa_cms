<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member', function (Blueprint $table) {
            $table->increments('ixMember');
            $table->string('sStudentNumber', 9)->nullable();
            $table->string('sRoc', 10)->nullable();
            $table->tinyInteger('iDepartment')->unsigned();
            $table->string('sPassword', 60);
            $table->string('sNameCn', 30);
            $table->string('sNameEn', 30);
            $table->tinyInteger('nGender')->unsigned();
            $table->date('dtBirthday');
            $table->string('sEmail', 30)->nullable();
            $table->string('sPhoneTw', 20)->nullable();
            $table->string('sPhonePor', 20)->nullable();
            $table->string('sAddressTw')->nullable();
            $table->string('sAddressPor')->nullable();
            $table->boolean('fActive');
            $table->timestamp('dtCreated');
            $table->timestamp('dtUpdated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member');
    }
}
