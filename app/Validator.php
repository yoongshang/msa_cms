<?php

namespace App;

class Validator
{
    /**
     * Validator for email-address
     *
     * @param  string $email
     * @return bool
     */
    public static function isEmailValid(string $email) : bool
    {
        if (empty($email)){
            return false;
        }

        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Validator for student number
     *
     * @param  string $number
     * @return bool
     */
    public static function isStudentNumberValid(string $number) : bool
    {
        if (empty($number)) {
            return false;
        }

        // TODO: Only suport to 107taiwan year
        $pattern = '/^(8[4-9]|9[0-9]|10[0-7])[1-4][0-9]{5}$/';
        if ( ! preg_match($pattern, $number)) {
            return false;
        }

        return true;
    }

    /**
     * Validator for identity card number taiwan
     * citizen or foreign
     *
     * @param  string $roc
     * @return bool
     */
    public static function isRocValid(string $roc) : bool
    {
        if (empty($roc)) {
            return false;
        }

        // available for citizen or foreign
        $pattern = '/^([A-Z][1-2]{1}[0-9]{8})$|^([A-Z][A-D]\d{8})$/';
        if ( ! preg_match($pattern, $roc)) {
            return false;
        }

        return true;
    }

    /**
     * Validator for phone number
     * ex: +886912345678 Valid
     *        0912345678 Valid
     *      091-2345-678 Valid
     * Almost support all number format.
     * Maximum length 20.
     *
     * @param  string $number
     * @return bool
     */
    public static function isPhoneNumberValid(string $number) : bool
    {
        if (empty($number)) {
            return false;
        }

        if (strlen($number) > 20) {
            return false;
        }

        $pattern = '/^[+]?\d{1,16}([-]?\d{1,16}){1,16}$/';
        if ( ! preg_match($pattern, $number)) {
            return false;
        }

        return true;
    }
}
