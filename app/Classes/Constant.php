<?php
/**
 * Class for key:value subject.
 */

namespace App\Classes;

class Constant
{
    private $code = 0;
    private $name = '';

    /**
     * Constant constructor.
     *
     * @param int $code
     * @param string $name
     */
    public function __construct(int $code = 0, string $name = '')
    {
        $this->setCode($code);
        $this->setName($name);
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCode() : int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}
