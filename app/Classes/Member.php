<?php

namespace App\Classes;

use stdClass;
use App\Validator;

class Member
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    private $ixMember = 0;
    private $sStudentNumber = '';
    private $sRoc = '';
    private $iDepartment = 0;
    private $sPassword = '';
    private $sNameCn = '';
    private $sNameEn = '';
    private $nGender = 0;
    private $dtBirthday = '';
    private $sEmail = '';
    private $sPhoneTw = '';
    private $sPhonePor = '';
    private $sAddressTw = '';
    private $sAddressPor = '';
    private $fActive = true;
    private $dtCreated = '';
    private $dtUpdated = null;


    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->ixMember = $id;
    }

    /**
     * @param string $number
     */
    public function setStudentNumber(string $number)
    {
        if (Validator::isStudentNumberValid($number)) {
            $this->sStudentNumber = $number;
        }
    }

    /**
     * @param string $roc
     */
    public function setRoc(string $roc)
    {
        if (Validator::isRocValid($roc)) {
            $this->sRoc = $roc;
        }
    }

    /**
     * @param int $id
     */
    public function setDepartment(int $id)
    {
        $this->iDepartment = $id;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->sPassword = $password;
    }

    /**
     * @param string $nameCn
     */
    public function setNameCn(string $nameCn)
    {
        $this->sNameCn = $nameCn;
    }

    /**
     * @param string $nameEn
     */
    public function setNameEn(string $nameEn)
    {
        $this->sNameEn = $nameEn;
    }

    /**
     * @param int $gender
     */
    public function setGender(int $gender)
    {
        if ($gender == self::GENDER_MALE or $gender == self::GENDER_MALE) {
            $this->nGender = $gender;
        }
    }

    /**
     * @param string $birthday
     */
    public function setBirthday(string $birthday)
    {
        $this->dtBirthday = $birthday;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        if (Validator::isEmailValid($email)) {
            $this->sEmail = $email;
        }
    }

    /**
     * @param string $number
     */
    public function setPhoneTw(string $number)
    {
        if (Validator::isPhoneNumberValid($number)) {
            $this->sPhoneTw = $number;
        }
    }

    /**
     * @param string $number
     */
    public function setPhonePor(string $number)
    {
        if (Validator::isPhoneNumberValid($number)) {
            $this->sPhonePor = $number;
        }
    }

    /**
     * @param string $address
     */
    public function setAddressTw(string $address)
    {
        $this->sAddressTw = $address;
    }

    /**
     * @param string $address
     */
    public function setAddressPor(string $address)
    {
        $this->sAddressPor = $address;
    }

    /**
     * @param bool $isActive
     */
    public function setActive(bool $isActive)
    {
        $this->fActive = $isActive;
    }

    /**
     * @param string $datetime
     */
    public function setCreatedTime(string $datetime)
    {
        $this->dtCreated = $datetime;
    }

    /**
     * @param string $datetime
     */
    public function setUpdatedTime(string $datetime)
    {
        $this->dtUpdated = $datetime;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->ixMember;
    }

    /**
     * @return string
     */
    public function getStudentNumber() : string
    {
        return $this->sStudentNumber;
    }

    /**
     * @return string
     */
    public function getRoc() : string
    {
        return $this->sRoc;
    }

    /**
     * @return int
     */
    public function getDepartment() : int
    {
        return $this->iDepartment;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->sPassword;
    }

    /**
     * @return string
     */
    public function getNameCn() : string
    {
        return $this->sNameCn;
    }

    /**
     * @return string
     */
    public function getNameEn() : string
    {
        return $this->sNameEn;
    }

    /**
     * @return int
     */
    public function getGender() : int
    {
        return $this->nGender;
    }

    /**
     * @return string
     */
    public function getBirthday() : string
    {
        return $this->dtBirthday;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->sEmail;
    }

    /**
     * @return string
     */
    public function getPhoneTw() : string
    {
        return $this->sPhoneTw;
    }

    /**
     * @return string
     */
    public function getPhonePor() : string
    {
        return $this->sPhonePor;
    }

    /**
     * @return string
     */
    public function getAddressTw() : string
    {
        return $this->sAddressTw;
    }

    /**
     * @return string
     */
    public function getAddressPor() : string
    {
        return $this->sAddressPor;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->fActive;
    }

    /**
     * @return string
     */
    public function getCreatedTime() : string
    {
        return $this->dtCreated;
    }

    /**
     * @return null
     */
    public function getUpdatedTime()
    {
        return $this->dtUpdated;
    }

    /**
     * @param stdClass $content
     */
    public function loadFromObject(stdClass $content)
    {
        if (isset($content->ixMember)) {
            $this->setId($content->ixMember);
        }
        if (isset($content->sStudentNumber)) {
            $this->setStudentNumber($content->sStudentNumber);
        }
        if (isset($content->sRoc)) {
            $this->setRoc($content->sRoc);
        }
        if (isset($content->iDepartment)) {
            $this->setDepartment($content->iDepartment);
        }
        if (isset($content->sPassword)) {
            $this->setPassword($content->sPassword);
        }
        if (isset($content->sNameCn)) {
            $this->setNameCn($content->sNameCn);
        }
        if (isset($content->sNameEn)) {
            $this->setNameEn($content->sNameEn);
        }
        if (isset($content->nGender)) {
            $this->setGender($content->nGender);
        }
        if (isset($content->dtBirthday)) {
            $this->setBirthday($content->dtBirthday);
        }
        if (isset($content->sEmail)) {
            $this->setEmail($content->sEmail);
        }
        if (isset($content->sPhoneTw)) {
            $this->setPhoneTw($content->sPhoneTw);
        }
        if (isset($content->sPhonePor)) {
            $this->setPhonePor($content->sPhonePor);
        }
        if (isset($content->sAddressTw)) {
            $this->setAddressTw($content->sAddressTw);
        }
        if (isset($content->sAddressPor)) {
            $this->setAddressPor($content->sAddressPor);
        }
        if (isset($content->fActive)) {
            $this->setActive($content->fActive);
        }
        if (isset($content->dtCreated)) {
            $this->setCreatedTime($content->dtCreated);
        }
        if (isset($content->dtUpdated)) {
            $this->setUpdatedTime($content->dtUpdated);
        }
    }
}
