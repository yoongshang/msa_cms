<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MemberModel
 *
 * @property int $ixMember
 * @property string|null $sStudentNumber
 * @property string|null $sRoc
 * @property int $iDepartment
 * @property string $sPassword
 * @property string $sNameCn
 * @property string $sNameEn
 * @property int $nGender
 * @property string $dtBirthday
 * @property string|null $sEmail
 * @property string|null $sPhoneTw
 * @property string|null $sPhonePor
 * @property string|null $sAddressTw
 * @property string|null $sAddressPor
 * @property int $fActive
 * @property string $dtCreated
 * @property string|null $dtUpdated
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereDtBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereDtCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereDtUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereFActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereIDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereIxMember($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereNGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSAddressPor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSAddressTw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSNameCn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSPhonePor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSPhoneTw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSRoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MemberModel whereSStudentNumber($value)
 * @mixin \Eloquent
 */
class MemberModel extends Model
{
    protected $table = 'Member';
    protected $primaryKey = 'ixMember';
    public $timestamps = false; // turn of automatic timestamp

    protected $fillable = ['sStudentNumber'
                        , 'sRoc'
                        , 'iDepartment'
                        , 'sPassword'
                        , 'sNameCn'
                        , 'sNameEn'
                        , 'nGender'
                        , 'dtBirthday'
                        , 'sEmail'
                        , 'sPhoneTw'
                        , 'sPhonePor'
                        , 'sAddressTw'
                        , 'sAddressPor'
                        , 'fActive'
                        , 'dtCreated'
                        , 'dtUpdated'];
}
